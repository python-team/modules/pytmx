Source: pytmx
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Dominik George <natureshadow@debian.org>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 pypy,
 pypy-setuptools,
 pypy-six,
 python3-all,
 python3-setuptools,
 python3-six,
Standards-Version: 4.4.1
Homepage: https://github.com/bitcraft/PyTMX
Testsuite: autopkgtest-pkg-python
Vcs-Git: https://salsa.debian.org/python-team/packages/pytmx.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pytmx

Package: python3-pytmx
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Enhances: python3-pygame
Description: Library to read Tiled Map Editor's TMX maps (Python 3)
 PyTMX is a map loader for python/pygame designed for games.  It provides
 smart tile loading with a fast and efficient storage base.  Not only does it
 correctly handle most Tiled object types, it also will load metadata for
 them so you can modify your maps and objects in Tiled instead of modifying
 your source code.
 .
 This package contains the Python 3 version.

Package: pypy-pytmx
Architecture: all
Depends:
 ${misc:Depends},
 ${pypy:Depends},
Enhances: pypy-pygame
Description: Library to read Tiled Map Editor's TMX maps (PyPy)
 PyTMX is a map loader for python/pygame designed for games.  It provides
 smart tile loading with a fast and efficient storage base.  Not only does it
 correctly handle most Tiled object types, it also will load metadata for
 them so you can modify your maps and objects in Tiled instead of modifying
 your source code.
 .
 This package contains the PyPy version.
